======================================================
Running the simulator as a function call
======================================================

See the tutorial on :ref:`MF-STRO-DF with simpleapp<maestro_tutorial_simpleapp>`
for more details on how to run the MF-STRO-DF algorithm where the simulator is
called using a function call
