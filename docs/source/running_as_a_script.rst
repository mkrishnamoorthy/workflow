======================================================
Running the simulator as a script run
======================================================

See the tutorial on :ref:`MF-STRO-DF with Monte Carlo simulator<maestro_tutorial_mc>`
for more details on how to run the MF-STRO-DF algorithm where the simulator is
called by running a script
